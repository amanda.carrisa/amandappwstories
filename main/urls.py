from django.conf.urls import url
from django.urls import path
from .views import *

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('story1',story1, name='story1'),
    path('story3',story3, name='story3'),
    path('story32',story32, name='story32'),

]
