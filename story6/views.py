from django.shortcuts import render, redirect

from .forms import KegiatanForm, PesertaForm
from story6.models import Kegiatan, Peserta


# Create your views here.
def story6(request):
    datas = {
        'kegiatan': Kegiatan.objects.all(), #semua objek yang ada di dalem model Kegiatan
        'peserta': Peserta.objects.all(), #semua objek yang ada di dalem model Peserta
    }
    data_forms = {
        'kegiatan_form': KegiatanForm(),    #untuk pake form
        'peserta_form': PesertaForm(),
    }
    response = {
        'forms': data_forms,    #untuk dikembaliin, yang dimasukin pas ngerender html ntar
        'data': datas
    }
    return render(request, 'story6.html', response)


def post_kegiatan(request):
    if request.method == 'POST':        #kan ngisi
        form = KegiatanForm(request.POST or None)
        if form.is_valid():
            data_form = form.cleaned_data   #proses data yg dimasukin kalo valid
            data = Kegiatan(nama=data_form['nama_kegiatan'])
            data.save() #di save
            return redirect('/story6/') #abis save redirect balik
        else:
            return redirect('/story6/') #langsung redirect
    else:
        return redirect('/story6/')


def post_peserta(request, id_kegiatan): #associated sama kegiatan
    if request.method == 'POST':
        form = PesertaForm(request.POST or None)    
        if form.is_valid():     #kalo valid isi formnya
            kegiatan = Kegiatan.objects.get(id=id_kegiatan) #ngambil dulu kegiatan
            data_form = form.cleaned_data
            data_input = Peserta()  #isi ke kegiatan
            data_input.nama = data_form['nama_peserta']
            data_input.kegiatan = kegiatan

            data_input.save()
            return redirect('/story6/')
        else:
            return redirect('/story6/')
    else:
        return redirect('/story6/')
