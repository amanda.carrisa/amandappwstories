from django.test import TestCase
from django.test import Client
from django.urls import resolve

from story6.models import Kegiatan, Peserta
from . import views


# Create your tests here.

class UnitTestForStory6(TestCase):
    def test_response_page(self): #ngembaliin alamat bener ga. 200
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):   #bener ga htmlnya itu story6
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_func_page(self):   #ngecek fungsi views itu kepake ngga
        found = resolve('/story6/')
        self.assertEqual(found.func, views.story6)

    def test_story6_save_kegiatan_a_POST_request(self): #kesimpen ke model ngga nama kegiatannya
        Client().post('/story6/post-kegiatan', data={'nama_kegiatan': 'Unit Test'})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 1)#kan masukin nama kegiatan, kan mau assert, masukin unit test

    def test_story6_save_peserta_a_POST_request(self): #cek kalo nyimpen peserta namanya kalo iya jadi count 1
        obj = Kegiatan.objects.create(nama='Unit Test2') 
        Client().post('/story6/post-peserta/' + str(obj.id), data={'nama_peserta': 'Bejo'}) #kan ada variable data data itu key nya nama peserta, kalo ada, nama peserta itu nilainya bejo
        jumlah = Peserta.objects.filter(nama='Bejo').count()
        self.assertEqual(jumlah, 1)

    def test_story6_model_kegiatan(self): #Dia ngecek objek modelnya bisa dicreate atau ngga, kalau bisa berarti pas di-get bisa ada objeknya
        Kegiatan.objects.create(nama='Test Kegiatan')
        kegiatan = Kegiatan.objects.get(nama='Test Kegiatan') #kalo bisa dibuat modelnya, pas diget harusnya ada
        self.assertEqual(str(kegiatan), 'Test Kegiatan')   

    def test_story6_kegiatan_form_invalid(self):
        Client().post('/story6/post-kegiatan', data={}) #gaada yg masuk form kegiatannya, jadi 0 countnya
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count() #pas filter trus gaada, countnya 0
        self.assertEqual(jumlah, 0) #assert equal 0 sm 0