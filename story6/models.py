from django.db import models


# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=30)  #tipe karakter max len 30

    def __str__(self):
        return self.nama   #return stringnya input


class Peserta(models.Model):
    nama = models.CharField(max_length=30) #variable untuk input
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.DO_NOTHING) #gabisa diapus, kalo mau apus pake models.CASCADE
    #variable kegiatan dipake diviews untuk connect ke kegiatan, jadi minta kegiatan dl baru save
    def __str__(self):
        return self.nama
